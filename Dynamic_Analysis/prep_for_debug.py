#!/usr/bin/env python 

import os, sys, subprocess, argparse
from androguard.core.bytecodes import apk

APIMONITOR_CONFIG           = r'/home/santoku/work/APIMonitor-beta/config/default_api_collection'
APIMONITOR_CONFIG_COMMENT   = '# added by me'
APIMONITOR_PATH             = r'/home/santoku/work/APIMonitor-beta/apimonitor.py'
JDBRC_PATH                  = r'/home/santoku/.jdbrc'
WORK_DIR                    = r'/home/santoku/host'

IMPLICIT_APIS = [
    '# intent related',
    'Landroid/content/Intent;->setClass',
    'Landroid/app/PendingIntent;->getBroadcast',
    '# intentFilter related',
    'Landroid/content/IntentFilter;-><init>',
    'Landroid/content/IntentFilter;->addAction',
    '# http request related',
    'Lorg/apache/http/client/methods/HttpPost;-><init>',
    'Lorg/apache/http/message/BasicNameValuePair;-><init>',
    'Lorg/apache/http/client/HttpClient;->execute',
    'Lorg/apache/http/client/methods/HttpGet;-><init>',
    '# phone related',
    'Landroid/telephony/TelephonyManager;->getNetworkOperatorName',
    'Landroid/telephony/TelephonyManager;->getLine1Number',
    'Landroid/telephony/TelephonyManager;->getNetworkType', 
    'Landroid/telephony/TelephonyManager;->getCellLocation',
    'Landroid/telephony/TelephonyManager;->listen',
    'Landroid/telephony/PhoneStateListener;-><init>',
    'Landroid/telephony/gsm/GsmCellLocation;->getCid',
    'Landroid/telephony/gsm/GsmCellLocation;->getLac',
    'Landroid/telephony/cdma/CdmaCellLocation;->getSystemId',
    'Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId',
    'Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId',
    'Landroid/telephony/TelephonyManager;->getDeviceId',
    '# sms related',
    'Landroid/telephony/gsm/SmsManager;->sendTextMessage',
    '# location related',
    'Landroid/location/LocationManager;->getBestProvider',
    'Landroid/location/LocationManager;->requestLocationUpdates',
    'Landroid/location/LocationManager;->getLastKnownLocation',
    'Landroid/location/Location;->getLongitude',
    'Landroid/location/Location;->getLatitude',
    '#shared preferences',
    'Landroid/content/SharedPreferences$Editor;->putString',
    'Landroid/content/SharedPreferences$Editor;->putInt',
    'Landroid/content/SharedPreferences$Editor;->putBoolean',
    'Landroid/content/SharedPreferences;->getString',
    'Landroid/content/SharedPreferences;->getInt',
    'Landroid/content/SharedPreferences;->getBoolean',
    '# methods of Context class',
    'Landroid/content/Context;->sendBroadcast',
    'Landroid/content/Context;->sendOrderedBroadcast',
    'Landroid/content/Context;->sendStickyBroadcast',
    'Landroid/content/Context;->sendStickyOrderedBroadcast',
    'Landroid/content/Context;->startActivity',
    'Landroid/content/Context;->startService',
    'Landroid/content/Context;->getSystemService',
    'Landroid/content/Context;->getSharedPreferences',
    '# AlarmManager related',
    'Landroid/app/AlarmManager;->setRepeating',
    '# process related',
    'Landroid/os/Process;->killProcess',
    '# MediaRecorder related',
    'Landroid/media/MediaRecorder;-><init>',
    'Landroid/media/MediaRecorder;->setAudioSource',
    'Landroid/media/MediaRecorder;->setOutputFormat',
    'Landroid/media/MediaRecorder;->setAudioEncoder',
    'Landroid/media/MediaRecorder;->setOutputFile',
    'Landroid/media/MediaRecorder;->start',
    '# FileIO related',
    'Ljava/io/File;-><init>',
    'Ljava/io/File;->exists',
    'Ljava/io/File;->mkdirs',
    'Ljava/io/File;->delete',
    'Ljava/io/File;->listFiles',
    'Ljava/io/File;->getName',
    'Ljava/io/DataInputStream;->available',
    'Ljava/io/DataInputStream;->readInt',
    'Ljava/io/DataInputStream;->readByte',
    'Ljava/io/ByteArrayOutputStream;->write',
    '# Network IO related',
    'Ljava/net/InetAddress;->getByName',
    'Ljava/net/Socket;->connect',
    'Ljava/net/Socket;->getOutputStream',
    'Ljava/net/Socket;->getInputStream',
    'Ljava/net/Socket;->isClosed',
    'Ljava/net/Socket;->isConnected',
    '# Thread related',
    'Ljava/lang/Thread;-><init>',
    'Ljava/lang/Thread;->start',
    '# external storage related',
    'Landroid/os/Environment;->getExternalStorageState',
    '# Exception related',
    'Ljava/lang/Exception;->getMessage',
    'Ljava/lang/InterruptedException;->printStackTrace',
    '# string related',
    'Ljava/lang/String;->equals',
    '# ContentObserver related',
    'Landroid/content/ContentResolver;->registerContentObserver',
    # 'Landroid/database/ContentObserver;->onChange',
    'Landroid/content/ContentResolver;->query',
    '# database related',
    'Landroid/database/Cursor;->moveToFirst',
    'Landroid/database/Cursor;->getCount',
    'Landroid/database/Cursor;->getColumnIndex',
    'Landroid/database/Cursor;->getInt',
    'Landroid/database/Cursor;->getString',
    'Landroid/database/Cursor;->getLong',
]

REFLECTION_APIS = [
    'Ljava/lang/Class;->forName',
    'Ljava/lang/Class;->getConstructors',
    'Ljava/lang/Class;->getDeclaredConstructors',
    'Ljava/lang/Class;->getFields',
    'Ljava/lang/Class;->getDeclaredFields',
    'Ljava/lang/Class;->getMethods',
    'Ljava/lang/Class;->getDeclaredMethods',
    'Ljava/lang/reflect/Method;->invoke',
]

def runcmd(cmdline):
    print 'Running [%s]...' % cmdline
    proc = subprocess.Popen(cmdline, close_fds=True, shell=True)
    proc.wait()

def createJDBRC(apkobj, apkbn):
    h = open(JDBRC_PATH, 'wb')
    # create breakpoints for each activity, broadcast receiver and service
    for activity in apkobj.get_activities():
        h.write('stop in %s.onCreate\n' % activity)
        h.write('stop in %s.onStart\n' % activity)
        h.write('stop in %s.onResume\n' % activity)
        h.write('stop in %s.onPause\n' % activity)
        h.write('stop in %s.onStop\n' % activity)
        h.write('stop in %s.onDestroy\n' % activity)
        
    for receiver in apkobj.get_receivers():
        h.write('stop in %s.onReceive\n' % receiver)
        
    for service in apkobj.get_services():
        h.write('stop in %s.onStart\n' % service)
        h.write('stop in %s.onCreate\n' % service)
 
    # create other breakpoints
    h.write('stop in android.telephony.SmsManager.sendTextMessage\n')
    
    h.write('use %s/%s-apktool-d/smali/\n' % (WORK_DIR, apkbn))
    h.close()   
    
def createAPIMonitorConfig(apkobj, onlyReflection=False):
    configContent = open(APIMONITOR_CONFIG, 'rb').read()
    pos = configContent.find(APIMONITOR_CONFIG_COMMENT)
    if pos != -1:
        configContent = configContent[:pos]
    if onlyReflection:
        configContent = APIMONITOR_CONFIG_COMMENT + '\n' + '\n'.join(REFLECTION_APIS)
    else:
        configContent += APIMONITOR_CONFIG_COMMENT + '\n' + '\n'.join(IMPLICIT_APIS) + '\n'.join(REFLECTION_APIS)
    open(APIMONITOR_CONFIG, 'wb').write(configContent)
    
parser = argparse.ArgumentParser(description='Prepare APKs for debug')

parser.add_argument('apkpath', help='path to apk')
parser.add_argument('-reflect', action='store_true', help='hook only reflection related APIs')
parser.add_argument('-jdb', action='store_true', help='create .jdbrc file')
parser.add_argument('-apimon', action='store_true', help='create apimonitor config file')

args = parser.parse_args()

apkpath = args.apkpath
apkbn = os.path.splitext(apkpath)[0]
print 'Preparing for debug [%s]' % apkpath

apkobj = apk.APK(apkpath) 

if args.apimon:
    createAPIMonitorConfig(apkobj, onlyReflection=True if args.reflect else False)
    # run apimonitor
    cmdline = r'%s -o %s-apimonitor %s' % (APIMONITOR_PATH, apkbn, apkpath)
    runcmd(cmdline)

    apkpath = '%s-apimonitor/%s_new.apk' % (apkbn, apkbn)

if args.jdb:
    # run apktool to decode in debug mode (-d option)
    cmdline = 'apktool d -d -f -o %s-apktool-d %s' % (apkbn, apkpath)
    runcmd(cmdline)
    print 'CREATED: %s_apktool-d' % apkbn
    
    # build for debug
    cmdline = 'apktool b -d -f -o %s_unalgn_dbg.apk %s-apktool-d' % (apkbn, apkbn)
    runcmd(cmdline)

    # generate key
    if not os.path.isfile('mykeystore.keystore'):
        cmdline = r'keytool -genkey -v -keystore mykeystore.keystore -alias my_key -keyalg RSA -keysize 2048 -validity 10000'
        runcmd(cmdline)
    # sign apk
    cmdline = r'jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore mykeystore.keystore %s_unalgn_dbg.apk my_key' % apkbn
    runcmd(cmdline)
    # align apk
    cmdline = r'zipalign -f -v 4 %s_unalgn_dbg.apk %s_dbg.apk' % (apkbn, apkbn)
    runcmd(cmdline)

    os.remove('%s_unalgn_dbg.apk' % apkbn)
    print 'CREATED: %s_dbg.apk' % apkbn
    createJDBRC(apkobj, apkbn)
    