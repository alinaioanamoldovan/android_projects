package lab4.daadsm.com.sms_final;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lab4.daadsm.com.sms_final.database.DbConstants;
import lab4.daadsm.com.sms_final.database.SMS;
import lab4.daadsm.com.sms_final.database.SmsDB;

import static lab4.daadsm.com.sms_final.database.SMS.RECEIVED;
import static lab4.daadsm.com.sms_final.database.SMS.SEND;

public class Conversation extends AppCompatActivity {

    ListView listView;
  //  CustomSimpleCursorAdapter adapter;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        Intent intent = getIntent();
        String text = intent.getStringExtra(ListSMS.Message);
        DifferentRowAdapter adapter = new DifferentRowAdapter(getList(text));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }



    public ArrayList<SMS> getList(String phone){
        SmsDB db = new SmsDB(this);
        cursor = db.getSmsPhone(phone);
        if (cursor == null){
            Toast.makeText(this, "Null",
                    Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(this, "Number" + cursor.getCount(),
                Toast.LENGTH_LONG).show();
        // adapter = new CustomSimpleCursorAdapter(this, R.layout.item_sms, cursor, fromColumns, toViews, 0);
        // listView = (ListView) findViewById(R.id.listViewConvo);
        // listView.setAdapter(adapter);
        ArrayList<SMS> smsArrayList = new ArrayList<>();

        if (cursor.moveToFirst()){
            do{
                String phoneNumber = cursor.getString(cursor.getColumnIndex(DbConstants.DbConstantsTable.COLUMN_PHONE));
                String messageBody = cursor.getString(cursor.getColumnIndex(DbConstants.DbConstantsTable.COLUMN_MESSAGE));
                String longDate = cursor.getString(cursor.getColumnIndex(DbConstants.DbConstantsTable.COLUMN_LONGDATE));
                int type = Integer.valueOf(cursor.getString(cursor.getColumnIndex(DbConstants.DbConstantsTable.COLUMN_TYPE)));
                SMS sms = new SMS(phoneNumber,messageBody,longDate,type);
                smsArrayList.add(sms);
                // do what ever you want here
            }while(cursor.moveToNext());
        }

        cursor.close();

        return smsArrayList;

    }

    public static class DifferentRowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<SMS> mList;

        public DifferentRowAdapter(List<SMS> list) {
            this.mList = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;

            switch (viewType) {
                case SEND:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_two, parent, false);
                    return new SendViewHolder(view);
                case RECEIVED:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_one, parent, false);
                    return new ReceivedViewHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            SMS object = mList.get(position);
            if (object != null) {
                switch (object.getType()) {
                    case SEND:
                        ((SendViewHolder) holder).mPhone.setText(object.getPhone());
                        ((SendViewHolder) holder).mBody.setText(object.getMessage());
                        ((SendViewHolder) holder).mTimeStamp.setText(object.getDate());
                        break;
                    case RECEIVED:
                        ((ReceivedViewHolder) holder).mPhone.setText(object.getPhone());
                        ((ReceivedViewHolder) holder).mBody.setText(object.getMessage());
                        ((ReceivedViewHolder) holder).mTimeStamp.setText(object.getDate());
                        break;
                }
            }
        }

        @Override
        public int getItemCount() {
            if (mList == null)
                return 0;
            return mList.size();
        }

        @Override
        public int getItemViewType(int position) {
            if (mList != null) {
                SMS object = mList.get(position);
                if (object != null) {
                    return object.getType();
                }
            }
            return 0;
        }

        public class SendViewHolder extends RecyclerView.ViewHolder {
            private TextView mPhone;
            private TextView mBody;
            private TextView mTimeStamp;
            public SendViewHolder(View itemView) {
                super(itemView);
                mPhone = (TextView) itemView.findViewById(R.id.phoneBody);
                mBody = (TextView)itemView.findViewById(R.id.messageBody);
                mTimeStamp =  (TextView)itemView.findViewById(R.id.timeStamp);
            }
        }

        public static class ReceivedViewHolder extends RecyclerView.ViewHolder {
            private TextView mPhone;
            private TextView mBody;
            private TextView mTimeStamp;

            public ReceivedViewHolder(View itemView) {
                super(itemView);
                mPhone = (TextView) itemView.findViewById(R.id.phoneBody);
                mBody = (TextView)itemView.findViewById(R.id.messageBody);
                mTimeStamp =  (TextView)itemView.findViewById(R.id.timeStamp);
            }
        }
    }

}
