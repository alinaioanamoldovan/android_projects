package lab4.daadsm.com.sms_final.database;

/**
 * Created by Alina on 11/15/2017.
 */

public class SMS {
    public static final int SEND = 1;
    public static final int RECEIVED = 2;
    public String phone;
    public String message;
    public String date;
    public int type;


    public SMS(String phone, String message, String date, int type) {
        this.phone = phone;
        this.message = message;
        this.date = date;
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
