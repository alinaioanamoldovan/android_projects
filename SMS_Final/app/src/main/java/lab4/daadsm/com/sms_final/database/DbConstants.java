package lab4.daadsm.com.sms_final.database;

import android.provider.BaseColumns;

/**
 * Created by Alina on 11/13/2017.
 */

public class DbConstants {

    public DbConstants() {
    }

    public  static class DbConstantsTable implements BaseColumns{
        public static final String TABLE_NAME = "SMS";
        public static final String COLUMN_PHONE = "PHONE";
        public static final String COLUMN_MESSAGE = "MESSAGE";
        public static final String COLUMN_TYPE = "TYPE";
        public static final String COLUMN_TIMESTAMP = "TS";
        public static final String COLUMN_SHORTDATE = "SHORTD";
        public static final String COLUMN_LONGDATE = "LONGD";
    }
}
