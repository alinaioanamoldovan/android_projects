package lab4.daadsm.com.sms_final.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alina on 11/13/2017.
 */

public class SmsDbHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "sms";

    public SmsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    String CREATE_INBOX_TABLE = "CREATE TABLE IF NOT EXISTS " + DbConstants.DbConstantsTable.TABLE_NAME + "("
            + DbConstants.DbConstantsTable._ID + " INTEGER PRIMARY KEY,"
            + DbConstants.DbConstantsTable.COLUMN_PHONE + " TEXT, "
            + DbConstants.DbConstantsTable.COLUMN_MESSAGE + " TEXT, "
            + DbConstants.DbConstantsTable.COLUMN_TYPE + " TEXT,"
            + DbConstants.DbConstantsTable.COLUMN_TIMESTAMP + " TEXT, "
            + DbConstants.DbConstantsTable.COLUMN_SHORTDATE + " TEXT, "
            + DbConstants.DbConstantsTable.COLUMN_LONGDATE + " TEXT" + ")";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_INBOX_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
