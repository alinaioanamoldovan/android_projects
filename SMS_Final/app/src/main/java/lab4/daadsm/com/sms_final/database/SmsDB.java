package lab4.daadsm.com.sms_final.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Alina on 11/13/2017.
 */

public class SmsDB {

    private SmsDbHelper dbHelper;

    public SmsDB(Context context) {
        this.dbHelper = new SmsDbHelper(context);
    }


    public long insertSendMessage(String phone, String message){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        String longDate=sdf.format(new Date());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String shortDate = sdf2.format(new Date());
        String type =  String.valueOf(1);
        String timeStamp = String.valueOf ((int)new Date().getTime()/1000);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(DbConstants.DbConstantsTable.COLUMN_PHONE,phone);
        value.put(DbConstants.DbConstantsTable.COLUMN_MESSAGE,message);
        value.put(DbConstants.DbConstantsTable.COLUMN_TYPE,type);
        value.put(DbConstants.DbConstantsTable.COLUMN_TIMESTAMP,timeStamp);
        value.put(DbConstants.DbConstantsTable.COLUMN_SHORTDATE,shortDate);
        value.put(DbConstants.DbConstantsTable.COLUMN_LONGDATE,longDate);

        long newId = db.insert(DbConstants.DbConstantsTable.TABLE_NAME,null,value);

        return newId;

    }
    public long insertReceivedMessage(String phone, String message){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        String longDate=sdf.format(new Date());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String shortDate = sdf2.format(new Date());
        String type =  String.valueOf(2);
        String timeStamp = (String.valueOf(new Date().getTime())) ;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(DbConstants.DbConstantsTable.COLUMN_PHONE,phone);
        value.put(DbConstants.DbConstantsTable.COLUMN_MESSAGE,message);
        value.put(DbConstants.DbConstantsTable.COLUMN_TYPE,type);
        value.put(DbConstants.DbConstantsTable.COLUMN_TIMESTAMP,timeStamp);
        value.put(DbConstants.DbConstantsTable.COLUMN_SHORTDATE,shortDate);
        value.put(DbConstants.DbConstantsTable.COLUMN_LONGDATE,longDate);

        long newId = db.insert(DbConstants.DbConstantsTable.TABLE_NAME,null,value);

        return newId;

    }

    public Cursor getSMS(){
        String type = String.valueOf(1);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT _id,phone,message,shortd FROM " + DbConstants.DbConstantsTable.TABLE_NAME +" s1"+ " WHERE "+" type = '"+ type+ "'" +" AND "+" timestamp = " + "(" + " SELECT MAX(timestamp) FROM "+DbConstants.DbConstantsTable.TABLE_NAME +" s2 "+"WHERE s2.phone = s1.phone)", null);
        return cur;
    }

    public Cursor getSMS2(){
        boolean val = true;
        String type = String.valueOf("2");
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projections = {DbConstants.DbConstantsTable._ID,
        DbConstants.DbConstantsTable.COLUMN_PHONE,DbConstants.DbConstantsTable.COLUMN_MESSAGE,DbConstants.DbConstantsTable.COLUMN_SHORTDATE};

        Cursor c = db.query(true,
                DbConstants.DbConstantsTable.TABLE_NAME,
                 projections,
                "type=?",                                // The columns for the WHERE clause
                new String[]{type},                                     // The values for the WHERE clause
                DbConstants.DbConstantsTable.COLUMN_PHONE,                                     // don't group the rows
                null,                                     // don't filter by row groups
                "ts DESC",null);

        return c;
    }
    public Cursor getSmsRight(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //String table = "table2";
        String[] columns = {"_id", "phone","message","shortd"};
        String selection = "ts  = ?";
        String[] selectionArgs = {"SELECT MAX(ts)\n" +
                "FROM "+ DbConstants.DbConstantsTable.TABLE_NAME+" s2\n" +
                "WHERE s2.phone = s1.phone"};
        //String groupBy = "phone";
        String having = null;
        String orderBy = "ts DESC";
        String limit = "1";

        Cursor cursor = db.query(DbConstants.DbConstantsTable.TABLE_NAME+" s1", columns, selection, selectionArgs, null, having, null, null);
        return cursor;
    }
    public Cursor getSmsAnother(){
        String type = String.valueOf(2);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT _id, phone, message, shortd\n" +
                "FROM "+ DbConstants.DbConstantsTable.TABLE_NAME+" s1\n" +
                "WHERE type ='"+ type +"' AND ts = (SELECT MAX(ts)\n" +
                "FROM sms s2\n" +
                "WHERE s2.phone = s1.phone)";

        Cursor c = db.rawQuery(sql,null);
        return c;
    }

    public Cursor getSmsPhone(String phoneNum){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT _id, phone, message, longd,type\n" +
                " FROM "+ DbConstants.DbConstantsTable.TABLE_NAME +
                " WHERE phone ='"+ phoneNum+"'"+" ORDER BY ts DESC";
        Cursor c = db.rawQuery(sql,null);

        return c;
    }

}
