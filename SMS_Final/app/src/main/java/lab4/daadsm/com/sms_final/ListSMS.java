package lab4.daadsm.com.sms_final;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import lab4.daadsm.com.sms_final.database.DbConstants;
import lab4.daadsm.com.sms_final.database.SmsDB;

public class ListSMS extends AppCompatActivity {

    public static final String Message = "message";

    ListView listView;
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sms);
        //deleteDatabase("sms");
        getData();

        FloatingActionButton fb = (FloatingActionButton)findViewById(R.id.fab);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),NewMessage.class);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t = (TextView) view.findViewById(R.id.phone);
                String s = t.getText().toString();
                Intent in = new Intent(view.getContext(),Conversation.class);
                in.putExtra(Message,s);
                startActivity(in);
            }
        });
    }

    public void onResume() {  // After a pause OR at startup
        super.onResume();
        getData();

    }

    public void newMessage(View v){

    }

    private void getData() {
        SmsDB db = new SmsDB(this);
        String[] fromColumns = {DbConstants.DbConstantsTable.COLUMN_PHONE,DbConstants.DbConstantsTable.COLUMN_MESSAGE,DbConstants.DbConstantsTable.COLUMN_SHORTDATE};
        int[] toViews = {R.id.phone,R.id.message,R.id.date};
        Cursor cursor = db.getSmsAnother();
        adapter = new SimpleCursorAdapter(this, R.layout.item_sms, cursor, fromColumns, toViews, 0);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

    }
}
