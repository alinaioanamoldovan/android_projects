package lab4.daadsm.com.sms_final;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import lab4.daadsm.com.sms_final.database.SmsDB;

/**
 * Created by Alina on 11/13/2017.
 */

public class SMSReceiver extends BroadcastReceiver{

    SmsDB db;
    @Override
    public void onReceive(Context context, Intent intent) {
        //Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
        db =  new SmsDB(context);

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);


                    Long id = db.insertReceivedMessage(phoneNumber,message);
                    if (id == null){
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(context,
                                "Not inserted in db", duration);
                        toast.show();
                    }
                    // Show Alert
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context,
                            "senderNum: "+ senderNum + ", message: " + message, duration);
                    toast.show();

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}
