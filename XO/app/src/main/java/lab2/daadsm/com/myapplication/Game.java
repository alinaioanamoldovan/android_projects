package lab2.daadsm.com.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Game extends AppCompatActivity {

    int c[][];
    Button[][] buttons;
    TextView textView;
    int activePlayer;
    boolean gameOver = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        int player = 0;
        Intent intent = getIntent();
        String firstPlayer = intent.getStringExtra(StartGame.GAMER1);
        String secondPlayer = intent.getStringExtra(StartGame.GAMER2);

        Random random = new Random();
        player = random.nextInt((2 - 1) + 1) + 1;
        activePlayer = player;
        textView = (TextView)findViewById(R.id.viewPlayer);
        if (player == 1) {
            textView.setText("The first player is " + firstPlayer + " with X");
            activePlayer=1;
        }else
        {
            if (player == 2){
                textView.setText("The first player is " + secondPlayer + " with O" );
                activePlayer = 2;
            }
        }

        buttons = new Button[3][3];
        //c = new int[3][3];

        buttons[0][0] = (Button)findViewById(R.id.first);
        buttons[0][1] = (Button)findViewById(R.id.second);
        buttons[0][2] = (Button)findViewById(R.id.third);
        buttons[1][0] = (Button)findViewById(R.id.forth);
        buttons[1][1] = (Button)findViewById(R.id.fifth);
        buttons[1][2] = (Button)findViewById(R.id.sixth);
        buttons[2][0] = (Button)findViewById(R.id.seventh);
        buttons[2][1] = (Button)findViewById(R.id.eight);
        buttons[2][2] = (Button)findViewById(R.id.ninth);


        Button buttonReset = (Button)findViewById(R.id.resetB);
        buttonReset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
        buttons[0][0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(buttons[0][0].getText().toString().equals("")){
                    if (activePlayer == 1){
                        textView.setText("It's second player turn");
                        activePlayer = 2;
                        buttons[0][0].setText("X");
                    }
                    else
                    {
                        if(activePlayer == 2){
                            textView.setText("It's first  player turn");
                            activePlayer = 1;
                            buttons[0][0].setText("O");
                        }
                    }
                }

                endGame();

            }
        });
        buttons[0][1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(buttons[0][1].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[0][1].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[0][1].setText("O");
                            }
                        }
                    }

                    endGame();

                }

        });
        buttons[0][2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(buttons[0][2].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[0][2].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[0][2].setText("O");
                            }
                        }
                    }

                    endGame();

                }

        });
        buttons[1][0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(buttons[1][0].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[1][0].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[1][0].setText("O");
                            }
                        }
                    }
                    endGame();

                }
        });
        buttons[1][1].setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                    if(buttons[1][1].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[1][1].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[1][1].setText("O");
                            }
                        }
                    }
                    endGame();

                }

        });
        buttons[1][2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(buttons[1][2].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[1][2].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[1][2].setText("O");
                            }
                        }
                    }
                    endGame();

                }

        });
        buttons[2][0].setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                    if(buttons[2][0].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[2][0].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[2][0].setText("O");
                            }
                        }
                    }
                    endGame();
                }

        });
        buttons[2][1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(buttons[2][1].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[2][1].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[2][1].setText("O");
                            }
                        }
                    }
                endGame();
            }
        });
        buttons[2][2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(buttons[2][2].getText().toString().equals("")){
                        if (activePlayer == 1){
                            textView.setText("It's second player turn");
                            activePlayer = 2;
                            buttons[2][2].setText("X");
                        }
                        else
                        {
                            if(activePlayer == 2){
                                textView.setText("It's first  player turn");
                                activePlayer = 1;
                                buttons[2][2].setText("O");
                            }
                        }
                    }

                    endGame();

                }

        });




    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem item = menu.add("Color Picker");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int initialColor = Color.WHITE;

        ColorPickerDialog colorPickerDialog = new ColorPickerDialog(this, initialColor, new ColorPickerDialog.OnColorSelectedListener() {

            public void onColorSelected(int color) {
                LinearLayout layout = (LinearLayout)findViewById(R.id.LinearLayout);
                layout.setBackgroundColor(color);
                setContentView(layout);
                showToast(color);
            }

        });
        colorPickerDialog.show();
        return true;
    }



    private void showToast(int color) {
        String rgbString = "R: " + Color.red(color) + " B: " + Color.blue(color) + " G: " + Color.green(color);
        Toast.makeText(this, rgbString, Toast.LENGTH_LONG).show();
    }



       public void endGame(){
           String a,b,c,d,e,f,g,h,i;

           a = buttons[0][0].getText().toString();
           b = buttons[0][1].getText().toString();
           c = buttons[0][2].getText().toString();
           d = buttons[1][0].getText().toString();
           e = buttons[1][1].getText().toString();
           f = buttons[1][2].getText().toString();
           g = buttons[2][0].getText().toString();
           h = buttons[2][1].getText().toString();
           i = buttons[2][2].getText().toString();

           if (a.equals(b) && a.equals(c) && a.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }
           if(d.equals(e) && d.equals(f) && d.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }
           if(g.equals(h) && g.equals(i) && g.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }

           if (a.equals(d) && a.equals(g) && a.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }

           if(b.equals(e) && b.equals(h) && b.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }

           if(c.equals(f) && c.equals(i) && c.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }

           if(a.equals(e) && a.equals(i) && a.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }


           if(c.equals(e) && c.equals(g) && c.equals("X")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("First player wins");
           }


           if (a.equals(b) && a.equals(c) && a.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }
           if(d.equals(e) && d.equals(f) && d.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }
           if(g.equals(h) && g.equals(i) && g.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }

           if (a.equals(d) && a.equals(g) && a.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }

           if(b.equals(e) && b.equals(h) && b.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }

           if(c.equals(f) && c.equals(i) && c.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }

           if(a.equals(e) && a.equals(i) && a.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }


           if(c.equals(e) && c.equals(g) && c.equals("O")){
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       buttons[k][j].setEnabled(false);
                   }
               }

               textView.setText("Second player wins");
           }
           else {
               int nrX=0;
               int nrO = 0;
               for(int k=0;k<3;k++){
                   for(int j=0;j<3;j++){
                       if (!(buttons[k][j].getText().toString().isEmpty())){
                            if(buttons[k][j].getText().toString().equals("X")){
                                nrX++;
                            }else {
                                nrO++;
                            }
                       }
                   }
               }

               if ((nrX > 3) || (nrO > 3)){
                   textView.setText("There is a draw");
                   for(int k=0;k<3;k++){
                       for(int j=0;j<3;j++){
                           buttons[k][j].setEnabled(false);
                       }
                   }
               }
           }




       }

       public void resetGame(){
           for(int i=0;i<3;i++){
               for(int j=0;j<3;j++){
                   buttons[i][j].setText("");
                   buttons[i][j].setEnabled(true);
               }
           }
       }

}
