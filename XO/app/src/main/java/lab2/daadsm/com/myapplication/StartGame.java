package lab2.daadsm.com.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StartGame extends AppCompatActivity {


    public static final String GAMER1 = "game1";
    public static final String GAMER2= "game2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem item = menu.add("Color Picker");
        return true;
    }
    public void sendMessage(View view){


        Button button = (Button)findViewById(R.id.startGame);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText)findViewById(R.id.firstPlayer);
                EditText editText1 = (EditText)findViewById(R.id.secondPlayer);
                String firstPlayer = editText.getText().toString();
                String secondPlayer = editText1.getText().toString();
                if ((firstPlayer.isEmpty()) || (secondPlayer.isEmpty())){
                    Toast.makeText(getApplicationContext(),"The name of the players cannot be null",Toast.LENGTH_LONG).show();
                }else{
                    Intent intent = new Intent(v.getContext(),Game.class);
                    //intent.setClass(context,)
                    intent.putExtra(GAMER1,firstPlayer);
                    intent.putExtra(GAMER2,secondPlayer);
                    startActivity(intent);
                }
            }
        });



     }
}
