#!/usr/bin/env python

import os, sys, argparse
from androguard.core.bytecodes import apk
from androguard.core.bytecodes import dvm
from androguard.core.analysis import analysis
from androguard.decompiler.dad import decompile

def getClassPath(className):
    # va returna calea unde va trebui salvata o anumita clasa
    # get_name() pentru un obiect ClassDefItem va returna un nume de clasa care 
    # va incepe cu "L" si se termina cu ";" ex: Ljava/lang/Object;
    return '%s.java' % className[1:-1]

def getClassName(className):
    # va returna numele unei clase
    # ex. pentru Ljava/lang/Object; va returna: java.lang.Object
    return className[1:-1].replace('/', '.')
    
def makeDirs(directory):
    # va crea un director cu toate subdirectoarele care inca nu exista
    if not os.path.isdir(directory):
        os.makedirs(directory)
                
def decompileMethod(methodObj, analysis):
    # <methodObj> e un obiect de tipul EncodedMethod
    # <analysis> e un obiect de tipul VMAnalysis
    if methodObj.get_code() == None:
        return None
    methodAnalysis = analysis.get_method(method)    # returns MethodAnalysis object
    decompMethod = decompile.DvMethod(methodAnalysis)
    try:
        decompMethod.process()
        methodSource = decompMethod.get_source()
    except:
        print 'Failed to decompile [%s]' % methodObj.get_name()
        return '''   method %s() {
        // failed to decompile
    }'''
    return methodSource
    
parser = argparse.ArgumentParser(description='Decompiler for APKs')

parser.add_argument('apkpath', help='path to apk')
parser.add_argument('-manifest', action='store_true', help='save AndroidManifest.xml')
parser.add_argument('-perms', action='store_true', help='list permissions')
parser.add_argument('-activities', action='store_true', help='list activities')
parser.add_argument('-decomp', action='store_true', help='decompile apk')

args = parser.parse_args()

apkpath = args.apkpath
apkobj = apk.APK(apkpath)

filename = os.path.basename(apkpath)
filenameList  = filename.split(".")
makeDirs(filenameList[0])
if args.manifest:
    # save manifest
	open(filenameList[0]+"/"+"AndroidManifest.xml", 'wb').write(apkobj.get_android_manifest_xml().toprettyxml(newl='', encoding='utf-8'))
    
    
if args.perms:
    # list permissions
	handle =  open(filenameList[0]+"/"+"permissions.txt", 'wb')
	for perms in apkobj.get_permissions():
    		handle.write(perms)
		handle.write("\n")
	handle.close()
    
if args.activities:
    # list activities
	handle = open(filenameList[0]+"/"+"activities.txt", 'wb')
	for act in apkobj.get_activities():
		handle.write(act)
		handle.write("\n")
   	handle.close()
    
if args.decomp:
    rawdex = apkobj.get_dex()
    dex = dvm.DalvikVMFormat(rawdex, decompiler='dad')
    analysis = analysis.VMAnalysis(dex)
    # enumerate classes
    # for each class enumerate interfaces, fields, query super class
    # for each class enumerate and decompile methods
    # write the java file to disk
    for class_obj in dex.get_classes():
        a= getClassPath(class_obj.get_name()).split("/")
	if len(a) > 2 :
		makeDirs(filenameList[0]+"/"+a[0]+"/"+a[1])
		handle = open(filenameList[0]+"/"+a[0]+"/"+a[1]+"/"+a[2], 'wb')
		className = a[2].split(".")
	elif len(a) == 2 :
		makeDirs(filenameList[0]+"/"+a[0])
		handle = open(filenameList[0]+"/"+a[0]+"/"+a[1], 'wb')
		className = a[1].split(".")
	else:
		makeDirs(filenameList[0]+"/"+"com/classes")
		handle = open(filenameList[0]+"/"+"com/classes/"+a[0], 'wb')
		className = a[0].split(".")

	flags = class_obj.get_access_flags_string() 
	super_classes =  class_obj.get_superclassname()
	classesList = getClassName(super_classes).rsplit(".")
	cInterface = class_obj.get_interfaces()
	if classesList:
		handle.write(flags+" "+ className[0]+" {")
	elif  cInterface :
		listC = cInterface[0].split("/")
		print listC[2].replace(";","")
		if (classesList[2] != "Object")  &  (classesList[2] != "Handler"):
	     		handle.write(flags+" "+ className[0]+" extends "+classesList[2] +" implements " + listC[2].replace(";","")+" {")
		else:
			handle.write(flags+" "+ className[0]+" implements " + listC[2].replace(";","")+" {")
	else:
		if (classesList[2] != "Object"):
	     		handle.write(flags+" "+ className[0]+" extends "+classesList[2]+" {")
	for method in class_obj.get_methods():
		source = decompileMethod(method,analysis)
		if source:
			handle.write(source)
	handle.write(" ")
	handle.write("}")
	handle.close()
   