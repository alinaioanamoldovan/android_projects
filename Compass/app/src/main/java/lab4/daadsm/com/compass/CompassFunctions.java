package lab4.daadsm.com.compass;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Alina on 11/17/2017.
 */

public class CompassFunctions implements SensorEventListener {

    public SensorManager sensorManager;
    public Sensor mField;
    public Sensor accMetro;
    public float azimuth = 0f;
    public float correctAzimuth = 0;
    public ImageView compassImg;

    public  float[] mGeomagnetic = new float[3];
    public  float[] mGravity = new float[3];

    public CompassFunctions(Context context) {
        sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        mField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accMetro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }
    public void  start() {
        sensorManager.registerListener(this,mField,SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this,accMetro,SensorManager.SENSOR_DELAY_GAME);
    }
    public void stop(){
        sensorManager.unregisterListener(this);
    }

    public void adjustImage(){
                if  (compassImg == null) {
                    return;
                }
                    Animation an = new RotateAnimation(correctAzimuth, azimuth,
                            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                            0.5f);
                    correctAzimuth = azimuth;

                    an.setDuration(100);
                    an.setRepeatCount(0);
                    an.setFillAfter(true);

                    compassImg.startAnimation(an);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.97f;

        synchronized (this) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

                mGravity[0] = alpha * mGravity[0] + (1 - alpha)
                        * event.values[0];
                mGravity[1] = alpha * mGravity[1] + (1 - alpha)
                        * event.values[1];
                mGravity[2] = alpha * mGravity[2] + (1 - alpha)
                        * event.values[2];

            }

            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {

                mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha)
                        * event.values[0];
                mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha)
                        * event.values[1];
                mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha)
                        * event.values[2];

            }

            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
                    mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimuth = (float)((Math.toDegrees(orientation[0])+360)%360);
              //  azimuth = orientation[0];
                adjustImage();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
