package lab4.daadsm.com.compass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {


    public CompassFunctions compassFunctions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        compassFunctions = new CompassFunctions(this);
        compassFunctions.compassImg = (ImageView) findViewById(R.id.main_image_hands);;
    }

    protected void onStart() {

        super.onStart();
        compassFunctions.start();
    }

    protected void onPause() {

        super.onPause();
        compassFunctions.stop();
    }
    protected void onResume() {

        super.onResume();
        compassFunctions.start();
    }
    protected void onStop() {

        super.onStop();
        compassFunctions.stop();
    }


}
