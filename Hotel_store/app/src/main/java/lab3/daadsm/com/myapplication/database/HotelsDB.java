package lab3.daadsm.com.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alina on 10/21/2017.
 */

public class HotelsDB {
    private HotelsDBHelper hDBHelper;


    public HotelsDB(Context context ) {
        hDBHelper = new HotelsDBHelper(context) ;
    }



    public long insertHotel ( String hotelname , String address ,
                              String webpage , String phone ) {
// Va obtine o instanta a bazei de date pentru scriere (un SQLLiteDatabase )
// Va crea un obiect de tipul ContentValues pentru transmiterea valorilor din fiecare coloana .
// Va apela metoda insert pe obiectul SQLLiteDatabase
// Va returna ID -ul randului inserat .

        SQLiteDatabase db = hDBHelper.getWritableDatabase();

        ContentValues value = new ContentValues();

        value.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_HOTELNAME,hotelname);
        value.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_ADDRESS,address);
        value.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_WEBPAGE,webpage);
        value.put(HotelsDBSchema.HotelsTable.COLUMN_NAME_PHONE,phone);

        long newId = db.insert(HotelsDBSchema.HotelsTable.TABLE_NAME,null,value);

        return newId;
    }
    public Cursor getHotels () {
// Va obtine o instanta a bazei de date pentru citire
// Va defini o lista de stringuri cu coloanele care vor fi interogate
// Va defini o ordine de sortare a unitatilor de cazare (ex. numele unitatii )
// Va apela metoda query a obiectului SQLLiteDatabase siva returna Cursor -ul returnat de acesta .

      SQLiteDatabase db = hDBHelper.getReadableDatabase();
        String[] projection = {
                HotelsDBSchema.HotelsTable._ID,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_HOTELNAME,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_ADDRESS,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_WEBPAGE,
                HotelsDBSchema.HotelsTable.COLUMN_NAME_PHONE
        };

        //Filter results WHERE "title" = 'My Title'
        String selection = HotelsDBSchema.HotelsTable.COLUMN_NAME_HOTELNAME + " = ?";
        //String[] selectionArgs = { "My Title" };

// How you want the results sorted in the resulting Cursor
       // String sortOrder = FeedEntry.COLUMN_NAME_SUBTITLE + " DESC";

        Cursor cursor = db.query(
                HotelsDBSchema.HotelsTable.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                                     // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );


        return cursor;
        /*List itemIds = new ArrayList<>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(HotelsDBSchema.HotelsTable._ID));
            itemIds.add(itemId);
        }
        cursor.close();

        */
    }

    public void deleteAll(){
        SQLiteDatabase db = hDBHelper.getWritableDatabase();
        db.execSQL("DELETE FROM "+ HotelsDBSchema.HotelsTable.TABLE_NAME);
    }


}
