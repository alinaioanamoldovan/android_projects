package lab3.daadsm.com.myapplication.database;

import android.provider.BaseColumns;

/**
 * Created by Alina on 10/21/2017.
 */

public class HotelsDBSchema {

    public HotelsDBSchema() {
    }

    public static class HotelsTable implements BaseColumns{
        public static final String TABLE_NAME = "hotels";
        public static final String COLUMN_NAME_HOTELNAME = "hotelname";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_WEBPAGE = "webpage";
        public static final String COLUMN_NAME_PHONE = "phone";

    }
}
