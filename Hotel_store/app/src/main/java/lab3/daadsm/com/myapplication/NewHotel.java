package lab3.daadsm.com.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import lab3.daadsm.com.myapplication.database.HotelsDB;

public class NewHotel extends AppCompatActivity implements View.OnClickListener {

    HotelsDB hotelsDB;
    Button button;
    EditText textView,textView1,textView2,textView3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_hotel);

        Intent intent = getIntent();
        hotelsDB = new HotelsDB(this);
        button  = (Button)findViewById(R.id.saveButton);

         textView = (EditText) findViewById(R.id.hotelName);
         textView1 = (EditText)findViewById(R.id.address);
         textView2 = (EditText)findViewById(R.id.webPage);
         textView3 = (EditText)findViewById(R.id.phone);

        button.setOnClickListener(this);


    }

    public void onClick(View v) {
         String s1 = textView.getText().toString();
         String s2 = textView1.getText().toString();
         String s3 = textView2.getText().toString();
         String s4 = textView3.getText().toString();

        if (v == button){
            if (s1.isEmpty() || s2.isEmpty() || s3.isEmpty() || s4.isEmpty()){
                Toast.makeText(getApplicationContext(), "The fields cannot be empty",
                        Toast.LENGTH_LONG).show();
            }else {
                Long value = hotelsDB.insertHotel(s1,s2,s3,s4);

                if (value!=null){

                    Toast.makeText(getApplicationContext(), "The value was inserted in the db",
                            Toast.LENGTH_LONG).show();

                }
                else {
                    Toast.makeText(getApplicationContext(), "Something went wrong",
                            Toast.LENGTH_LONG).show();
                }
            }

        }

    }
}
