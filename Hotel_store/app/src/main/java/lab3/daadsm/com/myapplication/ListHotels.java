package lab3.daadsm.com.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import lab3.daadsm.com.myapplication.database.HotelsDB;
import lab3.daadsm.com.myapplication.database.HotelsDBSchema;

public class  ListHotels extends AppCompatActivity {

    ListView listView;
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_hotels);

        getData();


    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        getData();


    }

    private void getData() {
        HotelsDB db = new HotelsDB(this);
        String[] fromColumns = {HotelsDBSchema.HotelsTable.COLUMN_NAME_HOTELNAME, HotelsDBSchema.HotelsTable.COLUMN_NAME_ADDRESS, HotelsDBSchema.HotelsTable.COLUMN_NAME_WEBPAGE, HotelsDBSchema.HotelsTable.COLUMN_NAME_PHONE};
        int[] toViews = {R.id.hotelN, R.id.addres, R.id.webpage, R.id.phonee};


        Cursor cursor = db.getHotels();

        adapter = new SimpleCursorAdapter(this, R.layout.item_hotel, cursor, fromColumns, toViews, 0);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

    }

    public void sendMessage(View view) {
        Button button = (Button) findViewById(R.id.newHotel);

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NewHotel.class);
                startActivity(intent);

            }
        });
    }

    public void click(View v){
        String selected;
        Toast toast;
        Intent intent;
        switch (v.getId()){
            case R.id.addres:
                selected = ((TextView) v.findViewById(R.id.addres)).getText().toString();

                List<String> items = Arrays.asList(selected.split("\\s*,\\s*"));

                double lat = Double.parseDouble(items.get(0));
                double lng = Double.parseDouble(items.get(1));

                // show map
                 intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr="+lat+","+lng));
                startActivity(intent);
                 toast = Toast.makeText(getApplicationContext(), selected, Toast.LENGTH_SHORT);
                toast.show();

                break;
            case R.id.webpage:
                selected = ((TextView) v.findViewById(R.id.webpage)).getText().toString();

                 intent= new Intent(Intent.ACTION_VIEW,Uri.parse(selected));
                startActivity(intent);

                 toast = Toast.makeText(getApplicationContext(), selected, Toast.LENGTH_SHORT);
                toast.show();
                break;
            case R.id.phonee:
                 selected = ((TextView) v.findViewById(R.id.phonee)).getText().toString();
                 intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + selected));
                if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(v.getContext(), Manifest.permission.CALL_PHONE)) {
                    return;
                }
                startActivity(intent);


                toast = Toast.makeText(getApplicationContext(), selected, Toast.LENGTH_SHORT);
                toast.show();
                break;



        }
    }

    public void delete(View v){
        Button button = (Button)findViewById(R.id.deleteAll);

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                HotelsDB db = new HotelsDB(v.getContext());
                db.deleteAll();
                getData();
            }
        });
    }
}
