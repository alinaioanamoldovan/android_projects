package lab4.daadsm.com.playlistdownloader;

/**
 * Created by Alina on 11/20/2017.
 */

public class Song {

    public String name;
    public boolean donwloaded;

    public Song(String name,boolean donwloaded) {
        this.name = name;
        this.donwloaded = donwloaded;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDonwloaded() {
        return donwloaded;
    }

    public void setDonwloaded(boolean donwloaded) {
        this.donwloaded = donwloaded;
    }
}
