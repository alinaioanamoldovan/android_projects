package lab4.daadsm.com.playlistdownloader;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ThreadFactory;

public class MainActivity extends AppCompatActivity {

    private int progressOverall;
    public int progress;
    private Handler handler = new Handler();
    public ProgressBar progressBar;
    public ListView listView;
    public PlayListAdapter playListAdapter;
    public DownloadReceiver mReceiver;
    public String urlPlayList = "http://10.0.2.2:8080/downloadPlaylist";
    public String urlSong = "http://10.0.2.2:8080/downloadMusic?basename=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        final Intent donwloadIntent = new Intent(this, DownloadService.class);
        donwloadIntent.putExtra("url", urlPlayList);
        donwloadIntent.putExtra("receiver", new DownloadReceiver(new Handler()));

        startService(donwloadIntent);
        //  Thread t = new Thread(){};

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                long id = view.getId();
                View parentView = (View) view.getParent();

                TextView text1 = (TextView) parentView.findViewById(R.id.songName);
                String text = text1.getText().toString();


                if (id == R.id.downloadButton) {
                    Intent downloadMusic = new Intent(MainActivity.this, DownloadService.class);
                    downloadMusic.putExtra("url", urlSong + text);
                    downloadMusic.putExtra("receiver", new DownloadReceiver(new Handler()));
                    startService(downloadMusic);
                }

                progressBar = (ProgressBar) parentView.findViewById(R.id.downloadProgress);
                if (progressBar == null) {
                    Toast.makeText(getApplicationContext(), "Null progress bar", Toast.LENGTH_LONG).show();
                }
                final int[] mProgressStatus = {0};
                Thread timer = new Thread() {
                    public void run() {
                        try {

                            while (mProgressStatus[0] < 100) {
                                progressOverall = progress;
                                System.out.println("Received " + progress);
                                sleep(1000);
                                mProgressStatus[0] += 30;
                                progressBar.setProgress(mProgressStatus[0]);

                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                        }
                    }
                };
                timer.start();

            }
        });


    }

    class DownloadReceiver extends ResultReceiver {

        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            super.onReceiveResult(resultCode, resultData);
            if (resultCode == DownloadService.UPDATE_PROGRESS) {
                progress = resultData.getInt("progress");

            }
            if (resultCode == DownloadService.PLAYLIST_READY) {
                // vom popula ListView-ul cu lista de melodii
                  ArrayList<Song> songs = new ArrayList<>();
                  ArrayList<String> strings = resultData.getStringArrayList("result");
                //  Toast.makeText(this,"The number of  songs" + strings.size(),Toast.LENGTH_LONG).show();
                File dir = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MUSIC),"/music/");
                List<File> files = getListFiles2(dir);
                for (int i = 0; i < strings.size(); i++) {
                    String str = strings.get(i);
                    boolean down = false;
                    for(int j=0;j<files.size();j++){
                        if (files.get(j).getName().equals(str))
                        {
                            down = true;
                        }
                    }
                    Song song = new Song(str, down);
                    songs.add(song);
                }
                playListAdapter = new PlayListAdapter(MainActivity.this, songs);
                listView.setAdapter(playListAdapter);
            }
        }
    }

    private List<File> getListFiles2(File parentDir) {
        List<File> inFiles = new ArrayList<>();
        Queue<File> files = new LinkedList<>();
        files.addAll(Arrays.asList(parentDir.listFiles()));
        while (!files.isEmpty()) {
            File file = files.remove();
            if (file.isDirectory()) {
                files.addAll(Arrays.asList(file.listFiles()));
            } else if (file.getName().endsWith(".mp3")) {
                inFiles.add(file);
            }
        }
        return inFiles;
    }


    }



            //View parentView = (View) getParent();

