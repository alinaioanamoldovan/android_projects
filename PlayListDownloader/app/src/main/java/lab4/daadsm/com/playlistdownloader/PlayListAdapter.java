package lab4.daadsm.com.playlistdownloader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Alina on 11/20/2017.
 */

public class PlayListAdapter extends ArrayAdapter<Song>{
    ArrayList<Song> songs;
    Context context;
    public PlayListAdapter(@NonNull Context context, ArrayList<Song> songs) {
        super(context, 0,songs);
        this.context = context;
        this.songs = songs;
    }

    public View getView(final int position, View converView, final ViewGroup parent){
        Song song = songs.get(position);
        //ViewHolder viewHolder;

        if (converView == null){
            converView = LayoutInflater.from(getContext()).inflate(R.layout.song_item, parent, false);
          //  holder = new ViewHolder();
        }

        TextView textView = (TextView)converView.findViewById(R.id.songName);
        if (textView == null){
            Toast.makeText(converView.getContext(),"Null song name",Toast.LENGTH_LONG).show();
        }
        textView.setText(song.name);
        ImageButton button = (ImageButton) converView.findViewById(R.id.downloadButton);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, R.id.downloadButton); // Let the event be handled in onItemClick()
            }
        });


        ProgressBar progressBar = (ProgressBar)converView.findViewById(R.id.downloadProgress);

        if (song.isDonwloaded() == true){
            progressBar.setProgress(100);
           button.setBackgroundResource(R.drawable.images);
           // imgB.setEnabled(true);
        }else{
            button.setBackgroundResource(R.drawable.download);
            progressBar.setVisibility(View.VISIBLE);
        }

        return converView;

    }
}
