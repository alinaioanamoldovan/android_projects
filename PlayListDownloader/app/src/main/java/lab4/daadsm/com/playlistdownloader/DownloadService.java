package lab4.daadsm.com.playlistdownloader;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Alina on 11/19/2017.
 */

public class  DownloadService extends IntentService {

    public static final int UPDATE_PROGRESS = 1000;
    public static final int PLAYLIST_READY = 2000;
    private int downloadLength;
    private int lastProgress;
    private ResultReceiver receiver;

    public DownloadService(){
        super(DownloadService.class.getName());


    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String urlToDownload = intent.getStringExtra("url");
        receiver = (ResultReceiver) intent.getParcelableExtra("receiver");

        if (urlToDownload.contains("basename")){
            downloadMusic(urlToDownload);
        }else {
            downloadPlaylist(urlToDownload);
        }

    }

    private void downloadPlaylist(String urlToDownload){
        InputStream inputStream = null;

        HttpURLConnection urlConnection = null;
        Bundle bundle = new Bundle();
        /* forming th java.net.URL object */
        URL url = null;
        try {
            url = new URL(urlToDownload);
            urlConnection = (HttpURLConnection) url.openConnection();
            int statusCode = urlConnection.getResponseCode();

            if (statusCode == 200){
                inputStream = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                ArrayList<String> result = new ArrayList<>();

                while ((line = bufferedReader.readLine()) != null) {
                    result.add(line);
                }

                if (null != result && result.size() > 0) {
                    bundle.putStringArrayList("result",result);
                    receiver.send(PLAYLIST_READY, bundle);
                }

            /* Close Stream */
                if (null != inputStream) {
                    inputStream.close();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    private void downloadMusic(String urlToDownload){


        Bundle b = new Bundle();
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlToDownload);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Toast.makeText(getApplicationContext(),"Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage(),Toast.LENGTH_LONG);
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();
            Uri uri = Uri.parse(urlToDownload);
            String name = uri.getQueryParameter("basename");

            // download the file
            input = connection.getInputStream();
            File musicDirectory = new
                    File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MUSIC), "music");
            if (!musicDirectory.exists())
                musicDirectory.mkdirs();
            File musicFile = new File(musicDirectory, name);
            output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MUSIC)+"/music/" + name);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                total += count;
                // publishing the progress....
                if (fileLength > 0) { // only if total length is known
                    b.putInt("progress", ((int) (total * 100 / fileLength)));
                    receiver.send(UPDATE_PROGRESS,b);
                }
                output.write(data, 0, count);
            }
        } catch (Exception e) {
             e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }

    }
}
